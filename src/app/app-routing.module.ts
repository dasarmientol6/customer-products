import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BankAccountsComponent } from './pages/bank-accounts/bank-accounts.component';
import { ReportsComponent } from './pages/reports/reports.component';

// Rutas de los modulos para redireccionamiento
const routes: Routes = [
  { path: 'bank-account', component: BankAccountsComponent },
  { path: 'reports', component: ReportsComponent },
  { path: '', redirectTo: '/bank-account', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
