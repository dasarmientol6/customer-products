import { BankAccount } from 'src/app/entities/bank-account';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { BankAccountsService } from 'src/app/services/bank-accounts.service';

import { Component } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent {

  /*
  Arrreglo que contiene la informacion bancaria de un cliente
  */
  private bankAccounts: BankAccount[];

  /*
  Indica el numero de cuentas inactivas
  */
  private inactiveAccounts: number;

  /*
  Indica el numero de cuentas activas
  */
  private activeAccounts: number;

  /*
  Indica el numero de cuentas de ahorro
  */
  private savingAccounts: number;

  /*
  Indica el numero de cuentas corriente
  */
  private currentAccounts: number;

  /*
  Contiene la configuracion de datos del grafico
  */
  public barChartData: ChartDataSets[];

  /*
  Contiene la configuracion de datos del grafico
  */
  public barChartDataType: ChartDataSets[];

  /*
  Labels del grafico
  */
  public barChartLabels: Label[];

  /*
  Labels del grafico
  */
  public barChartLabelsType: Label[];

  /*
  Tipo del grafico
  */
  public barChartType: ChartType;


  public barChartLegend: boolean;

  /*
  Plugins del grafico
  */
  public barChartPlugins: [];

  /*
  Opciones del grafico
  */
  public barChartOptions: ChartOptions;

  /**
   * Constructor de la clase
   *
   * @param bankAccountsService Sevicio que contiene los metodos para obtener la informacion de las cuentas bancarias
   *
   * @author Diego Sarmiento - Nov, 10 - 2021
   */
  constructor(
    private bankAccountsService: BankAccountsService
  ) {
    this.inactiveAccounts = 0;
    this.activeAccounts = 0;
    this.barChartData = [];
    this.barChartDataType = [];
    this.loadBankAccount();
    this.barChartType = 'bar';
    this.barChartLegend = true;
    this.barChartPlugins = [];
    this.barChartOptions = {
      responsive: true
    };
    this.savingAccounts = 0;
    this.currentAccounts = 0;
  }

  /**
   * Metodo que busca las cuentas bancarias
   *
   * @author Diego Sarmiento - Nov, 10-2021
   */
  public async loadBankAccount() {
    await this.bankAccountsService
      .getBankAccounts()
      .then((response) => {
        console.log('response', response);

        // Se obtinen las cuentas bancarias
        this.bankAccounts = response['accounts'];

        // Se recorren todas las cuentas y se revisa el estado de la cuenta
        for (const bankAccount of this.bankAccounts) {
          // Se guardan los resultados de las cuentas, si hay activas o inactivas
          if (bankAccount.status === 'activa') {
            this.activeAccounts = this.activeAccounts + 1;
          } else {
            this.inactiveAccounts = this.inactiveAccounts + 1;
          }

          // Se guardan los resultados de las cuentas, si hay de ahorros o corrientes
          if (bankAccount.type === 'Ahorros') {
            this.savingAccounts = this.savingAccounts + 1;
          } else {
            this.currentAccounts = this.currentAccounts + 1;
          }
        }

        // Labels para indicar los estados de las cuentas (Con 2 labels no funciona, por lo tanto se ha dejado un label vacio)
        this.barChartLabels = ["Activas", "Inactivas", ""];

        this.barChartLabelsType = ["Ahorros", "Corriente", ""];

        // Datos del grafico
        this.barChartData = [
          { data: [this.activeAccounts, this.inactiveAccounts, 0], label: 'Estados de las cuentas' }
        ];

        // Datos del grafico
        this.barChartDataType = [
          { data: [this.savingAccounts, this.currentAccounts, 0], label: 'Tipos de cuentas' }
        ];
      })
      .catch((error) => {
        console.error('Error: ', error);
      });
  }

}
