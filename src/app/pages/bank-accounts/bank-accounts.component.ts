import { BankAccount } from 'src/app/entities/bank-account';
import { BankAccountsService } from 'src/app/services/bank-accounts.service';

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-bank-accounts',
  templateUrl: './bank-accounts.component.html',
  styleUrls: ['./bank-accounts.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*', minHeight: "*" })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class BankAccountsComponent implements OnInit {

  /**
   * Arrreglo que contiene la informacion bancaria de un cliente
   */
  private bankAccounts: BankAccount[];

  /*
  Arreglo para indicar las columnas de la tabla de la interfaz
  */
  public displayedColumns: string[];

  /*
  Objeto que contiene la informacion de datos y configuracion de la tabla
  */
  public dataSource: MatTableDataSource<BankAccount>;

  /*
  Ordenamiento de la tabla
  */
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  /*
  Paginador de la tabla
  */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  expandedElement: BankAccount | null;

  /**
   * Constructor de la clase
   *
   * @param bankAccountsService Sevicio que contiene los metodos para obtener la informacion de las cuentas bancarias
   *
   * @author Diego Sarmiento - Nov, 10 - 2021
   */
  constructor(
    private bankAccountsService: BankAccountsService,
  ) {
    this.bankAccounts = [];
  }

  ngOnInit(): void {
    this.loadBankAccount();
  }

  /**
   * Metodo que busca las cuentas bancarias
   *
   * @author Diego Sarmiento - Nov, 10-2021
   */
  public async loadBankAccount() {
    await this.bankAccountsService
      .getBankAccounts()
      .then((response) => {
        console.log('response', response);

        // Se obtinen los datos las cuentas
        this.bankAccounts = response['accounts'];

        // Se configura la tabla de la interfaz
        // Columnas a mostrarse en la tabla
        this.displayedColumns = ['name'];
        // Objeto que estructura la informacion de la tabla
        this.dataSource = new MatTableDataSource<BankAccount>(this.bankAccounts);
        // Paginador de la tabla
        this.dataSource.paginator = this.paginator;
        // Datos que se van a mostrar en la tabla
        this.dataSource.data = this.bankAccounts;
        // Ordenamiento de la tabla
        this.dataSource.sort = this.sort;
      })
      .catch((error) => {
        console.error('Error: ', error);
      });
  }

  /**
   * Metodo que realiza la busqueda en la tabla
   *
   * @author Diego Sarmiento - Nov, 10-2021
   */
  public filterTable(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
