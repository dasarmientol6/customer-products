import { BankAccount } from './../entities/bank-account';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BankAccountsService {

  /**
   * Constructor de la clase
   *
   * @param http Servicio para enviar la peticion por http
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Metodo que obtiene las cuentas bancarias
   *
   * @returns Arreglo con cuentas bancarias
   * @author Diego Sarmiento - Nov, 10-2021
   */
  public getBankAccounts(): Promise<Array<BankAccount>> {
    return this.http.get<Array<BankAccount>>(environment.api.base).toPromise();
  }
}
