import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare const jQuery: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'customer-products';

  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Nov, 10-2021
   */
  constructor(
    private router: Router,
  ) { }

  /**
   * Metodo para cambiar de pagina en la app
   *
   * @param event Evento donde se obtiene el numero de la pagina
   * @author Diego Sarmiento - Nov, 10-2021
   */
  public changeTab(event) {

    // Dependiendo la tab seleccionada, se redirige al componente
    if (event.index === 0) {
      this.router.navigateByUrl('/bank-account');
    } else if (event.index === 1) {
      this.router.navigateByUrl('/reports');
    }
  }

}
