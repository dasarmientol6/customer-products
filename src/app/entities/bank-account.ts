export class BankAccount {

    public balance: string;
    public name: string;
    public number: string;
    public status: string;
    public type: string;

    constructor() {
        this.balance = null;
        this.name = null;
        this.number = null;
        this.status = null;
        this.type = null;
    }
}

